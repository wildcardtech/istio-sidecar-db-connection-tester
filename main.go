package main

import (
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"os"
	"time"
)

const istiosidecarreadyurl = "http://localhost:15021/healthz/ready"
const httpclienttimeout = 1 * time.Second
const httpclientmaxtries = 30

var envDbHost string
var envDbName string
var envDbUsername string
var envDbPassword string

var valueDbHost string
var valueDbName string
var valueDbUsername string
var valueDbPassword string

var okHost, okName, okUsername, okPassword bool

var httpTries int = 0

func init()  {
	flag.StringVar(&envDbHost, "dbhost", "DB_HOST", "ENV-Key for DB-Host")
	flag.StringVar(&envDbName, "dbname", "DB_NAME", "ENV-Key for DB-Name")
	flag.StringVar(&envDbUsername, "dbusername", "DB_USERNAME", "ENV-Key for DB-Username")
	flag.StringVar(&envDbPassword, "dbpassword", "DB_PASSWORD", "ENV-Key for DB-Password")
}

func main() {
	flag.Parse()

	fmt.Println("=====================")
	fmt.Println(" Istio Sidecar Probe")
	fmt.Println("=====================")
	fmt.Println()

	fmt.Println("--------")
	fmt.Println(" Values")
	fmt.Println("--------")

	valueDbHost, okHost = os.LookupEnv(envDbHost)
	if !okHost {
		fmt.Println(envDbHost + " is not present")
	} else {
		fmt.Printf("%s: %s\n", envDbHost, valueDbHost)
	}

	valueDbName, okName = os.LookupEnv(envDbName)
	if !okName {
		fmt.Println(envDbName + " is not present")
	} else {
		fmt.Printf("%s: %s\n", envDbName, valueDbName)
	}

	valueDbUsername, okUsername = os.LookupEnv(envDbUsername)
	if !okUsername {
		fmt.Println(envDbUsername + " is not present")
	} else {
		fmt.Printf("%s: %s\n", envDbUsername, valueDbUsername)
	}

	valueDbPassword, okPassword = os.LookupEnv(envDbPassword)
	if !okPassword {
		fmt.Println(envDbPassword + " is not present")
	} else {
		fmt.Printf("%s: %s\n", envDbPassword, valueDbPassword)
	}

	fmt.Println()

	if okHost && okName && okUsername && okPassword {
		fmt.Println("Values complete.")
	} else {
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		fmt.Println("!! One or more values missing. Check pod configuration !!")
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		os.Exit(-1)
	}

	fmt.Println()

	fmt.Println("----------------------------------------------")
	fmt.Println(" Waiting for Istio Sidecar to become ready...")
	fmt.Println("----------------------------------------------")

	for {
		fmt.Print(".")

		client := http.Client{
			Timeout: httpclienttimeout,
		}
		response, err := client.Get(istiosidecarreadyurl)
		if err == nil {
			if response.StatusCode == 200 {
				break
			}
		}

		time.Sleep(1 * time.Second)

		httpTries++
		if httpTries >= httpclientmaxtries {
			fmt.Println()
			fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
			fmt.Println("!! Istio Sidecar did not become not ready after 30 seconds !!")
			fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
			os.Exit(-2)
		}
	}
	fmt.Println("Istio Sidecar ready.")

	fmt.Println()

	fmt.Println("---------------------------")
	fmt.Println(" Connecting to database...")
	fmt.Println("---------------------------")

	db, err := sql.Open("mysql", valueDbUsername + ":" + valueDbPassword + "@tcp(" + valueDbHost + ":3306)/" + valueDbName)
	if err != nil {
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		fmt.Println("!! Cannot connect to database !!")
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		fmt.Println(err.Error())
		os.Exit(-3)
	}

	fmt.Println("Connection to database successful.")

	err = db.Close()
	if err != nil {
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		fmt.Println("!! Cannot close to database !!")
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		fmt.Println(err.Error())
		os.Exit(-4)
	}

	os.Exit(0)
}
