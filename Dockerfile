FROM golang:1.16-alpine3.13 AS builder

COPY . /go/src/dbconntest

RUN cd /go/src/dbconntest && go install && CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -a -tags netgo -ldflags '-w' -o dbconntest main.go

FROM alpine:3.13

RUN mkdir /service
COPY --from=builder /go/src/dbconntest/dbconntest /service

CMD ["/service/dbconntest"]

WORKDIR /service
